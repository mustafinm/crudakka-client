package api

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.marshalling.Marshal
import akka.http.scaladsl.model.Uri.Query
import akka.http.scaladsl.model._
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.ActorMaterializer
import models.Phone

import scala.concurrent.Future

/**
  * Created by murat on 23.05.17.
  */
trait PhoneApi extends PhoneRepository {

  import api.Marshalling._

  private val baseUrl = "http://localhost:9000/phones"

  implicit val system = ActorSystem()
  implicit val materializer = ActorMaterializer()

  import system.dispatcher

  val http = Http()
  val / = "/"

  override def create(phone: Phone): Future[Phone] = {
    val future = Marshal(phone).to[RequestEntity]
    future.map {
      re => HttpRequest(HttpMethods.POST, Uri(baseUrl), entity = re)
    }.flatMap(http.singleRequest(_))
      .flatMap { res =>
        Unmarshal(res.entity).to[Phone]
      }
  }

  override def findAll(offset: Option[Int], limit: Option[Int], asc: Option[String], desc: Option[String]): Future[Seq[Phone]] = {
    val params = List("offset" -> offset, "limit" -> limit, "asc" -> asc, "desc" -> desc)
      .collect { case (k, v) if v.isDefined => (k, v.get.toString) }
    val uri = Uri(baseUrl).withQuery(Query(params: _*))
    http.singleRequest(HttpRequest(uri = uri)).flatMap { res =>
      Unmarshal(res.entity).to[Seq[Phone]]
    }
  }

  override def findByName(name: String): Future[Seq[Phone]] = {
    val uriName = name.replace(" ", "%20")
    http.singleRequest(HttpRequest(uri = Uri(baseUrl + / + uriName))).flatMap { res =>
      Unmarshal(res.entity).to[Seq[Phone]]
    }
  }

  override def update(phone: Phone): Future[Phone] = {
    if (phone.id.isEmpty) return Future.failed(new IllegalArgumentException("No id"))
    val future = Marshal(phone).to[RequestEntity]
    future.map {
      re => HttpRequest(HttpMethods.POST, Uri(baseUrl + / + phone.id.get), entity = re)
    }.flatMap(http.singleRequest(_))
      .flatMap { res =>
        Unmarshal(res.entity).to[Phone]
      }
  }

  override def delete(id: Long): Future[Boolean] = {
    http.singleRequest(HttpRequest(HttpMethods.DELETE, uri = Uri(baseUrl + / + id))).map(_.status match {
      case StatusCodes.NoContent => true
      case _                     => false
    })
  }
}
