package api

import models.Phone

import scala.concurrent.Future

/**
  * Created by musta on 2017-05-19.
  */
trait PhoneRepository {

  def create(phone: Phone): Future[Phone]
  def findAll(offset: Option[Int], limit: Option[Int], asc: Option[String], desc: Option[String]): Future[Seq[Phone]]
  def findByName(name: String): Future[Seq[Phone]]
  def update(phone: Phone): Future[Phone]
  def delete(id: Long): Future[Boolean]

}
