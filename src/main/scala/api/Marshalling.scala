package api

import akka.http.scaladsl.marshalling.{Marshaller, ToEntityMarshaller}
import akka.http.scaladsl.model.{ContentTypes, MediaTypes}
import akka.http.scaladsl.unmarshalling.{FromEntityUnmarshaller, Unmarshaller}
import models.Phone
import org.json4s.ext.JodaTimeSerializers
import org.json4s.native.JsonMethods._
import org.json4s.{DefaultFormats, Extraction, Formats}

/**
  * Created by musta on 2017-05-23.
  */
object Marshalling {

  implicit val formats = DefaultFormats ++ JodaTimeSerializers.all

  implicit def phoneMarshalling: ToEntityMarshaller[Phone] = {
    Marshaller
      .StringMarshaller
      .wrap(MediaTypes.`application/json`) {
        t: Phone =>
            compact(render(Extraction.decompose(t)))
      }
  }

  implicit def phoneUnmarshalling(implicit formats: Formats): FromEntityUnmarshaller[Phone] = {
   Unmarshaller
      .byteStringUnmarshaller
     .forContentTypes(ContentTypes.`application/json`)
     .mapWithCharset((data, charset) => {
       val json = data.decodeString(charset.nioCharset.name)
       parse(json).extract[Phone]
     })
  }

  implicit def phonesUnmarshalling(implicit formats: Formats): FromEntityUnmarshaller[Seq[Phone]] = {
   Unmarshaller
      .byteStringUnmarshaller
     .forContentTypes(ContentTypes.`application/json`)
     .mapWithCharset((data, charset) => {
       val json = data.decodeString(charset.nioCharset.name)
       parse(json).extract[Seq[Phone]]
     })
  }

}
