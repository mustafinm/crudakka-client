package console

import models.Phone

import scala.util.Try

/**
  * Created by musta on 2017-05-19.
  */
sealed trait Command {
  val cmd: String
}

object Command{
  val commands = List("create", "find", "findAll", "update", "delete")

  val validators = Map(
    "create" -> new CreateValidation(),
    "find" -> new FindValidation(),
    "findAll" -> new FindAllValidation(),
    "update" -> new UpdateValidation(),
    "delete" -> new DeleteValidation()
  )

}

case class Create(phone: Phone) extends Command {
  override val cmd: String = "create"
}

case class Find(name: String) extends Command {
  override val cmd: String = "find"
}

case class FindAll(offset: Option[Int], limit: Option[Int], asc: Option[String], desc: Option[String]) extends Command {
  override val cmd: String = "findAll"
}

case class Update(phone: Phone) extends Command {
  override val cmd: String = "update"
}

case class Delete(id: Long) extends Command {
  override val cmd: String = "delete"
}

case class CommandValues(name: String, params: List[String], options: Map[String, String])

trait Validation{
  val NO_CMD = "No such command: type `help` to view commands"
  val NO_ARGS = "Wrong number of arguments: type `help` to view commands"
  val WRONG_USAGE = "Wrong usage: "

  val template: String

  def valid(cmdVals: CommandValues): Either[String, Unit]
  def parse(cmdVals: CommandValues): Command
}

class CreateValidation extends Validation{
  override val template: String = "create `name` `phone`"

  override def valid(cmdVals: CommandValues): Either[String, Unit]  = {
    val CommandValues(name, params, op) = cmdVals
    if(!Command.commands.contains(name)) Left(NO_CMD)
    else if(params.size != 2) Left(NO_ARGS)
    else Right()
  }

  override def parse(cmdVals: CommandValues): Command = {
    val CommandValues(name, params, op) = cmdVals
    Create(Phone(None, params.head, params.tail.head))
  }
}


class FindValidation extends Validation{
  override val template: String = "find `name`"

  override def valid(cmdVals: CommandValues): Either[String, Unit]  = {
    val CommandValues(name, params, op) = cmdVals
    if(!Command.commands.contains(name)) Left(NO_CMD)
    else if(params.size != 1) Left(NO_ARGS)
    else Right()
  }

  override def parse(cmdVals: CommandValues): Command = Find(cmdVals.params.head)
}

class FindAllValidation extends Validation{
  override val template: String = "findAll [-o `offsetInt`] [-l `limitInt`] [-asc `asc`] [-desc `desc`]"

  override def valid(cmdVals: CommandValues): Either[String, Unit]  = {
    val CommandValues(name, params, op) = cmdVals
    val validatedOps = validateOps(op)
    if(!Command.commands.contains(name)) Left(NO_CMD)
    else if(validatedOps.isDefined) Left(validatedOps.get)
    else Right()
  }

  private def validateOps(options: Map[String, String]): Option[String] = {
    if(options.nonEmpty){
      val offsetFailed = cantInt(options.get("-o"))
      val limitFailed = cantInt(options.get("-l"))

      val available = List("-o", "-l", "-asc", "-desc")
      if(!options.keySet.exists(op => available.contains(op))){
        Some("Wrong options: available -o, -l, -asc, -desc")
      }else if(offsetFailed){
        Some(s"Option offset must be a number")
      }else if(limitFailed){
        Some(s"Option limit must be a number")
      }else{
        None
      }
    }else None
  }

  private def cantInt(op: Option[String]): Boolean = op.exists(o => Try(o.toInt).isFailure)

  override def parse(cmdVals: CommandValues): Command = {
    val CommandValues(name, params, op) = cmdVals
    val offset = op.get("-o").map(_.toInt)
    val limit = op.get("-l").map(_.toInt)
    val asc = op.get("-asc")
    val desc = op.get("-desc")
    FindAll(offset, limit, asc, desc)
  }
}

class UpdateValidation extends Validation{
  override val template: String = "update `id` `name` `phone`"

  override def valid(cmdVals: CommandValues): Either[String, Unit]  = {
    val CommandValues(name, params, op) = cmdVals
    if(!Command.commands.contains(name)) Left(NO_CMD)
    else if(params.size != 3) Left(NO_ARGS)
    else if(Try(params.head.toInt).isFailure) Left("Id must be a number")
    else Right()
  }

  override def parse(cmdVals: CommandValues): Command = {
    val CommandValues(name, params, op) = cmdVals
    Update(Phone(Some(params.head.toInt), params(1), params(2)))
  }
}

class DeleteValidation extends Validation{
  override val template: String = "delete `id`"

  override def valid(cmdVals: CommandValues): Either[String, Unit]  = {
    val CommandValues(name, params, op) = cmdVals
    val triedInt = Try(params.head.toInt)
    if(!Command.commands.contains(name)) Left(NO_CMD)
    else if(params.size != 1) Left(NO_ARGS)
    else if(triedInt.isFailure) Left("Id must be number")
    else Right()
  }

  override def parse(cmdVals: CommandValues): Command = {
    Delete(cmdVals.params.head.toInt)
  }
}


