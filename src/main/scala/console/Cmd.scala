package console

import api.PhoneRepository
import models.Phone

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

/**
  * Created by musta on 2017-05-19.
  */
trait Cmd {
  rep: PhoneRepository =>

  val EXIT = "exit"
  val HELP = "help"

  val wordsRegex = "[^\\s\"']+|\"([^\"]*)\"|'([^']*)'".r

  def execute(st: String): Future[Output] = {
    if (st == "exit") System.exit(0)
    if (st == "help") return Future.successful(helpText)

    parse(st) match {
      case Left(err)  => Future.successful(Output(err))
      case Right(cmd) =>
        val res = cmd match {
          case Create(phone)          => rep.create(phone)
          case FindAll(offset, limit, asc, desc) => rep.findAll(offset, limit, asc, desc)
          case Find(name)             => rep.findByName(name)
          case Update(phone)          => rep.update(phone)
          case Delete(id)             => rep.delete(id).map(b => if(b) "deleted" else "not exist")
        }
        res.map {
          case l: List[_] =>
            l.mkString("\n")
          case any      =>
            any.toString
        }.map(Output)
    }
  }

  val helpText: Output = {
    val cmdTemplates = Command.validators.values.map(_.template).mkString("\n")

    Output("----Available commands----\n" + cmdTemplates)

  }


  private def cmd(name: String, params: List[String], options: List[(String, String)]): Either[String, Command] = {
    val cmdVals = CommandValues(name, params, options.toMap)

    Command.validators.get(name) match {
      case Some(validator) =>
        validator.valid(cmdVals) match {
          case Left(e)  => Left(e + "\nUsage: " + validator.template)
          case Right(_) => Right(validator.parse(cmdVals))
        }
      case None            => Left("No such command: type `help` to view commands")
    }

  }

  private def findOptions(args: List[String]): List[(String, String)] = {
    if (args.nonEmpty)
      args.zip(args.tail).filter { case (op, _) => op.startsWith("-") }
    else Nil
  }

  def parse(st: String): Either[String, Command] = {

    val split = wordsRegex.findAllMatchIn(st.trim()).map(_.toString().replace("\"","")).toList
    if(split.isEmpty) return Left("No such command")
    val name = split.head
    val indexOfFirstOpt = split.tail.indexWhere(_.startsWith("-"))

    val params = if (indexOfFirstOpt != -1) split.tail.take(indexOfFirstOpt)
                 else                       split.tail
    val options = findOptions(split.tail)

    cmd(name, params, options)
  }


  case class Output(st: String)

}