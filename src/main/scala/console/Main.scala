package console

import api.PhoneApi

import scala.io.StdIn
import scala.concurrent.ExecutionContext.Implicits._
/**
  * Created by musta on 2017-05-19.
  */
object Main extends App{

  val cmd = new Cmd with PhoneApi

  @volatile var running = true

  while(running){
    val line = StdIn.readLine()
    cmd.execute(line).foreach(out => println(out.st))
  }


}
