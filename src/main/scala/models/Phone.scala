package models

import org.joda.time.DateTime

/**
  * Created by musta on 2017-05-19.
  */
case class Phone(id: Option[Long], name: String, phone: String, createdAt: Option[DateTime] = None){
  override def toString: String = s"id=${id.getOrElse("none")}, name=$name, phone=$phone, createdAt=${createdAt.getOrElse("none")}"
}