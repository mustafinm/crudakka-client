name := "crudakka-client"

version := "1.0"

scalaVersion := "2.11.8"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-http" % "10.0.3",
  "org.json4s" %% "json4s-ext" % "3.3.+",
  "org.json4s" %% "json4s-native" % "3.3.+"
)